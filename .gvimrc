
:if has("terminfo")
   :  set t_Co=16
   :  set t_AB=<Esc>[%?%p1%{8}%<%t%p1%{40}%+%e%p1%{92}%+%;%dm
   :  set t_AF=<Esc>[%?%p1%{8}%<%t%p1%{30}%+%e%p1%{82}%+%;%dm
:else
   :  set t_Co=16
   :  set t_Sf=<Esc>[3%dm
   :  set t_Sb=<Esc>[4%dm
:endif

:filetype plugin on
:syntax on
:filetype indent on

:set mouse=a
:set clipboard+=unnamedplus
:set number
:set guifont=Hack\ Regular\ 12

colorscheme industry
