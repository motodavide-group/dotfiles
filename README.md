# These are my personal .dotfiles

This is my public repository to back up my own file and to share them
with whoever is looking for some inspiration.

The configuration files are published withowt any warranty, whoever use them
does so at their own risk.

## Software list

- bspwm
- Qtile
	- picom
	- nitrogen
	- redshift
	- dex
- sxhkd
- polybar
- alacritty
- zsh
- vim 
- gvim
- btop
- CopyQ
- flameshot
- rofi
- Qutebrowser
- amfora
- galculator
- pystopwatch
