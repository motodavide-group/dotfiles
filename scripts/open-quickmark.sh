#!/bin/bash
# Simple script to open a qutebrowser quickmark from the command line

# Select from quickmark file and remove everything except last word,
# which is the URL.
# At the moment it doesn't distinguish between URLs and not, so if it's 
# used for parsing search queries it ignores the last word
URL=$(cat ~/.config/qutebrowser/quickmarks | rofi -dmenu | sed -e 's/^.*\ //')
#echo $URL;
[ ! -z "$URL" ] && qutebrowser --target window "$URL" &
