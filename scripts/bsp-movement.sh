#!/bin/sh
# Simple script for using the same keybinding for window movement (tiled and floating)

case $1 in
	h)
		bspc node focused.tiled -s west || bspc node focused.floating -v -80 0 ;;
	j)
		bspc node focused.tiled -s south || bspc node focused.floating -v 0 80 ;;
	k)
		bspc node focused.tiled -s north || bspc node focused.floating -v 0 -80 ;;
	l)
		bspc node focused.tiled -s east || bspc node focused.floating -v 80 0 ;;
esac
