#/bin/sh
# Short script to toggle between my 2 analog ports

active_port=$(pactl list sinks | grep 'Active Port:' | sed s/Active\ Port:\ //)

[ $active_port = "analog-output-headphones" ] && pactl set-sink-port @DEFAULT_SINK@ analog-output-speaker
[ $active_port = "analog-output-speaker" ] && pactl set-sink-port @DEFAULT_SINK@ analog-output-headphones
