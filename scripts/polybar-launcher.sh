#! /bin/sh

# Terminate running istances
killall -q polybar

# Wait the process to shut down
while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done

# Launch polybar with default configuration
polybar davbar --config=~/.config/polybar/config.ini &
