#!/bin/bash

if [ -z "$(grep "$(hyprctl activewindow -j | jq -r ".class")" w-class-list.txt )" ]; then
	hyprctl dispatch killactive ""
else
	xdotool getactivewindow windowunmap
fi
