### MY PERSONAL PROMPT
PROMPT='%F{5}%n%f@%F{4}%m%f%# > '
RPROMPT='[%F{2}%~%f]'
PROMPT2='> '

#PROMPT='%F{color5}%n%f@%F{color4}%m%f%# > '
#RPROMPT='[%F{#10ba00}%~%f]'
#PROMPT2='> '

### VIM-KEYS IN ZSH
bindkey -v
export KEYTIMEOUT=1

### ENVIRONMENTAL VARIABLES
path+=('/home/davide/scripts/')
export PATH

### ALIASES 
alias server="ssh myserver"
alias cloud="ssh mycloud"
alias up-site="scp -r $HOME/documents/mysite/* mycloud:/var/www/mysite/; scp -r $HOME/documents/mycapsule/* mygemini:/home/gemini/gemini/"
alias dietpi="ssh dietpi"

alias syu="paru -Syu --noconfirm; flatpak update -y"

alias ls="ls -F -h --color=always -v --time-style=long-iso"
alias ll="ls -alh"

alias .="pwd"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

alias backup="sudo /home/davide/scripts/backup.sh"

alias sw-nvd="optimus-manager --switch nvidia --no-confirm"
alias sw-int="optimus-manager --switch intel --no-confirm"
alias sw-hyb="optimus-manager --switch hybrid --no-confirm"

alias config="/usr/bin/git --git-dir=$HOME/backups/dotfiles --work-tree=$HOME"

alias weather="curl wttr.in/Padua"

alias bal="ledger -f ~/documents/personal/bookkeeping/my-book-2023.ledger --price-db ~/documents/personal/bookkeeping/prices.pricedb -V -p 'this month' balance ^Assets ^Liabilities"
alias cf="ledger -f ~/documents/personal/bookkeeping/my-book-2023.ledger --price-db ~/documents/personal/bookkeeping/prices.pricedb -V -p 'this month' balance ^Expenses ^Income"
alias inv="ledger -f ~/documents/personal/bookkeeping/my-book-2023.ledger --price-db ~/documents/personal/bookkeeping/prices.pricedb -V -p 'this month' balance | tail -n 1"
alias lg="ledger -f ~/documents/personal/bookkeeping/my-book-2023.ledger --price-db ~/documents/personal/bookkeeping/prices.pricedb -V"

#alias cp="cp -i"
#alias mv="mv -i"
#alias rm="rm -i"
#alias ln="ln -i"

#alias plz="sudo $(fc -ln -1)"
alias plz="sudo !!"

alias wifi-list="iwctl station wlan0 scan && iwctl station wlan0 get-networks "
alias wifi-connect="iwctl station wlan0 connect "

### STOCK PRICES
# usage: tickerp <TICKER>
# site not working
#tickerp()
#{
#	curl terminal-stocks.herokuapp.com/"$1"
#}

### SEARCH ENGLISH DICTIONARY
dict()
{
	curl dict://dict.org/d:"$1"
}

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;; 
      *.rar)       unrar x $1   ;; 
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;      
      *.lzma)	   lzma -d $1	;;
      *.xpi)	   7z x $1		;;
      *.exe)	   7z x $1		;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
