" Syntax highlighting and autoindentation
:filetype plugin on
:syntax on
:filetype indent on

au BufNewFile,BufRead /*.rasi setf css

" Mouse support
:set mouse=a
if !has('nvim')
	:set ttymouse=sgr
endif

" Using the system clipboard as default
:set clipboard+=unnamedplus

" Show absolute line number in insert  mode and not focused state
" while show relative line number in normal mode
:set number

:augroup numbertoogle
:	autocmd!
:	autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu | endif
: 	autocmd BufLeave,FocusLost,InsertEnter,WinLeave	* if &nu | set nornu | endif
:augroup END

" Make tabs and indentation 4 stops wide
:set tabstop=4
:set shiftwidth=4

" Set hotkeys for spellchecking 
map <F5> :setlocal spell! spelllang=it<CR>
map <F6> :setlocal spell! spelllang=en_gb<CR>
map <F7> :setlocal spell! spelllang=en_us<CR>

" Set date and time auto-insertion
inoremap <F2> <C-R>=strftime('%F')<CR>
inoremap <F3> <C-R>=strftime('%T')<CR>
